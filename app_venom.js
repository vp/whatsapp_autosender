// Supports ES6
// import { create, Whatsapp } from 'venom-bot';
const venom = require('venom-bot');
const fs = require('fs');
const { sleep } = require('sleep');

// NUMERI - MESSAGGIO - MEDIA
args = process.argv.slice(2);

numeri = []

try {
    // read contents of the file
    const data = fs.readFileSync(args[0], 'UTF-8');

    // split the contents by new line
    const lines = data.split(/\r?\n/);

    // print all lines
    lines.forEach((line) => {
        if (line.length >= 10) {
            console.log(line);
            numeri.push(line); //if line length is at least 10, then save it
        }
    });
} catch (err) {
    console.error(err);
}

console.log("Ho letto "+numeri.length+" numeri")

var messaggio = "";

fs.readFile(args[1], 'utf-8', function (err, data) {
        if (err)
            throw err;

        // Converting Raw Buffer to text
        // data using tostring function.

        messaggio = data.toString();
    });
venom
  .create()
  .then((client) => send(client));
  

async function send(client) {

    total_OK = 0;

    for await( numero of numeri) {
        numero = numero.trim()
        numero += "@c.us"
        numero = "39" + numero
        await client
            .sendText(numero, messaggio)
            .then((result) => {
                if (result["status"] == "OK") {
                    total_OK += 1;
                }
                console.log('Result: ', result); //return object success
            })
            .catch((erro) => {
                console.error('Error when sending: ', erro); //return object error
            });

    if (args.length == 3) {
        client
            .sendFile(
                numero,
                args[2],
                'image-name'
            )
            .then((result) => {

                if (result["status"] == "OK") {
                    total_OK += 1;
                }

                console.log('Result: ', result); //return object success
            })
            .catch((erro) => {
                console.error('Error when sending: ', erro); //return object error
            });
    }
    }

    console.log("Ho inviato "+total_OK+" messaggi a fronte di " + numeri.length + " numeri letti");
    console.log("Percentuale di messaggi inviata : "+total_OK/numeri.length*100+"%");
}